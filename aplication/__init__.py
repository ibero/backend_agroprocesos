from flask import Flask
from flask_socketio import SocketIO

from .database.postgresql import initialize_db

socketio = SocketIO()


def create_app(debug=False):
    app = Flask(__name__)
    app.debug = debug
    app.config['SECRET_KEY'] = 'd3v3l0p'
    app_ctx = app.app_context()
    app_ctx.push()

    # blueprints
    from .telemetry import telemetry as telemetry_bp
    app.register_blueprint(telemetry_bp)

    # database
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://root:r00t@localhost:5432/agroprocesos'
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    initialize_db(app)

    socketio.init_app(app, cors_allowed_origins="*")
    return app

