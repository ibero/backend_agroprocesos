from ...database.postgresql import db, ma


class TelemetryModel(db.Model):
    __tablename__ = 'telemetry'

    # definir la tabla
    id = db.Column(db.Integer, primary_key=True)
    ip = db.Column(db.String(36))
    temperature = db.Column(db.Integer)
    humidity = db.Column(db.Integer)

    def __init__(self, ip, temperature, humidity):
        self.ip = ip
        self.temperature = temperature
        self.humidity = humidity

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_by_id(id):
        return TelemetryModel.query.get(id)

    @staticmethod
    def get_all():
        all_telemetries = TelemetryModel.query.all()
        return telemetries_schema.dump(all_telemetries)


# Schema
class TelemetrySchema(ma.Schema):
    # se definen los campos que se van a obtener cada vez que se interactue con el esquema
    class Meta:
        fields = ('ip', 'temperature', 'humidity')


telemetry_schema = TelemetrySchema()
telemetries_schema = TelemetrySchema(many=True)
