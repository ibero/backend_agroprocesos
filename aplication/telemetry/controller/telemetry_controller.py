from flask import jsonify, request
from ... import socketio
from ..domain.telemetry_model import TelemetryModel as tm


class TelemetryController:

    @staticmethod
    def get_telemetry():
        return jsonify({
            "telemetry": tm.get_all()
        }), 200

    @staticmethod
    def add_telemetry():
        ip = request.json['ip']
        temperature = request.json['temp']
        humidity = request.json['humidity']

        new_telemetry = tm(ip, temperature, humidity)

        socketio.emit('telemetry', request.json)

        response = new_telemetry.save()
        return jsonify({
            "id": response
        })
