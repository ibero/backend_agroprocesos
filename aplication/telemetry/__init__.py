from flask import Blueprint

telemetry = Blueprint('telemetry', __name__)

from .network import telemetry_network
