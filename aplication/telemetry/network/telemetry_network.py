from .. import telemetry
from ..controller.telemetry_controller import TelemetryController

tc = TelemetryController


@telemetry.route('/telemetry', methods=['GET'])
def get():
    return tc.get_telemetry()


@telemetry.route('/telemetry', methods=['POST'])
def post():
    return tc.add_telemetry()
