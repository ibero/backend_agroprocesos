from app import app
from aplication.database.postgresql import db
from aplication.telemetry.domain import telemetry_model

with app.app_context():
    db.create_all()
