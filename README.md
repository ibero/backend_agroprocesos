# Backend  Agroprocesos

## Requerimientos

- Python 3.9
- PIP
- Flask (https://flask.palletsprojects.com/en/2.0.x/)
- Flask-SocketIO (https://flask-socketio.readthedocs.io/en/latest/)
- Flask-SQLAlchemy (https://flask-sqlalchemy.palletsprojects.com/en/2.x/)
- Flask-Marshmallow (https://flask-marshmallow.readthedocs.io/en/latest/)
- Flask-Migrate (https://flask-migrate.readthedocs.io/en/latest/index.html)
- marshmallow-sqlalchemy
- psycopg2

## Inicialización
- Asegurarse de que las librerías se encuentren debidamente instaladas.
- Instalar docker en su equipo [tutorial](https://www.youtube.com/watch?v=NVvZNmfqg6M).
- Ejecutar el comando _docker-compose up -d_.
- Acceda a la base de datos y cree la base de datos **agroprocesos**.
- Dentro de la carpeta, ejecutar el comando _python3 manager.py_ ó desde pycharm pueden ejecutar el archivo (esto inicializará la base de datos).
- Ejecutar el comando _Flask run_.
- Validar que el servidor se encuentre funcionando consultando http://127.0.0.1:5000/telemetry

## Estructura de carpetas
- **_application_** : _es aquella que contiene los archivos  y la estructura de carpetas necesarios_ 
  - **_database_** : _se encarga de inicializar las librerías de la base de datos_
  - **_telemetry_** : _carpeta ejemplo que maneja la estructura de peticiones que se realicen a través del endpoint **telemetry**._
    - **_controller_**: _estructura de carpeta donde se manejarán todos los **controladores** (lógica de negocio de la app)_
    - **_domain_**: _estructura de carpeta donde se manejan los modelos y relaciones de la **base de datos**._
    - **_network_**: _estructura de carpeta donde se maneja las rutas de la aplicación_
  - **_app.py_** : _archivo que inicia la aplicación_
- migrations: _contiene los datos de migración de la base de datos_
- **_postgres_data_**: _archivo que necesita docker para el manejo de la base de datos._